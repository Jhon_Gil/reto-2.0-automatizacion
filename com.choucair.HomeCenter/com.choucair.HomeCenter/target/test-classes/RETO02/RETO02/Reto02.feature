@reporde_de_dias_pendientes_maxtime
Feature: Regresion

  @Reto02.1
  Scenario Outline: 1.	Mediante un escenario de caso exitoso crear una cuenta demo de forma exitosa, realizar una aserción con el mensaje final generado
    Given Ingresar al aplicativo Registro de Cuenta Demo
    And Registrar Nombre "<Primer_Nombre>"
    And Registrar Apellido "<Apellido>"
    And Registrar Pais Recidencia "<Pais_Residencia>"
    And Registrar Ciudad "<Ciudad>"
    And Registrar Telefono "<Telefono>"
    And Registrar Correo "<Correo>"
    And Registrar Idioma Preferido "<Idioma_Preferido>"
    And Registrar Tipo Plataforma "<Tipo_Plataforma>"
    And Registrar Tipo Tipo Cuenta "<Tipo_Cuenta>"
    And Registrar Tipo Moneda Base "<Moneda_Base>"
    And Registrar Tipo Apalancamiento "<Apalancamiento>"
    And Registrar Tipo Cantidad inversion "<Cantidad_inversion>"
    And Registrar Contrasena "<Contrasena>"
    And Registrar Confirmacion Contraseña "<Confirmacion_Contrasena>"
 	Then el verifica que la cuenta se halla creado 

    Examples: 
    |Primer_Nombre|Apellido|Pais_Residencia|Ciudad|Telefono|Correo|Idioma_Preferido|Tipo_Plataforma|Tipo_Cuenta|Moneda_Base|Apalancamiento|Cantidad_inversion|Contrasena|Confirmacion_Contrasena|
|Pedro|Perez|Colombia|Bogota|+57 5702223|jg@gmail.com|Ruso|MT4 (forex, CFDs sobre índices bursátiles, metales, energías)|XM Ultra Low Micro (1 lote=1,000)|USD|1:400|25.000|Bogo789*|Bogo789*|

  @Reto02.2
  Scenario Outline: 2.Mediante un escenario de caso exitoso crear una cuenta demo de forma exitosa, en el desarrollo del robot 
  se debe incluir el llenado erróneo de un campo en específico , validar su aparición y posteriormente su corrección. Usar el campo Primer Nombre.
  
    Given Ingresar al aplicativo Registro de Cuenta Demo
    And Registrar Nombre "<Primer_Nombre>"
    And Registrar Apellido "<Apellido>"
     And Error Campo Nombre "<Nombre_Correcto>"
    And Registrar Pais Recidencia "<Pais_Residencia>"
    And Registrar Ciudad "<Ciudad>"
    And Registrar Telefono "<Telefono>"
    And Registrar Correo "<Correo>"
    And Registrar Idioma Preferido "<Idioma_Preferido>"
    And Registrar Tipo Plataforma "<Tipo_Plataforma>"
    And Registrar Tipo Tipo Cuenta "<Tipo_Cuenta>"
    And Registrar Tipo Moneda Base "<Moneda_Base>"
    And Registrar Tipo Apalancamiento "<Apalancamiento>"
    And Registrar Tipo Cantidad inversion "<Cantidad_inversion>"
    And Registrar Contrasena "<Contrasena>"
    And Registrar Confirmacion Contraseña "<Confirmacion_Contrasena>"
 	Then el verifica que la cuenta se halla creado 
 	
    Examples: 
    |Primer_Nombre|Nombre_Correcto|Apellido|Pais_Residencia|Ciudad|Telefono|Correo|Idioma_Preferido|Tipo_Plataforma|Tipo_Cuenta|Moneda_Base|Apalancamiento|Cantidad_inversion|Contrasena|Confirmacion_Contrasena|
|ñoño|Jose|Perez|Colombia|Bogota|+57 5702223|jg@gmail.com|Ruso|MT4 (forex, CFDs sobre índices bursátiles, metales, energías)|XM Ultra Low Micro (1 lote=1,000)|USD|1:400|25.000|Bogo789*|Bogo789*|
 
 
 @Reto02.3
  Scenario Outline:3.Mediante un escenario de caso orientado al error intentar crear una cuenta con contraseñas diferentes. 
  Se debe validar la aparición de los siguiente mensajes en su correspondiente orden mediante la implementación de aserciones: 
  
   Given Ingresar al aplicativo Registro de Cuenta Demo
    And Registrar Nombre "<Primer_Nombre>"
    And Registrar Apellido "<Apellido>"
    And Registrar Pais Recidencia "<Pais_Residencia>"
    And Registrar Ciudad "<Ciudad>"
    And Registrar Telefono "<Telefono>"
    And Registrar Correo "<Correo>"
    And Registrar Idioma Preferido "<Idioma_Preferido>"
    And Registrar Tipo Plataforma "<Tipo_Plataforma>"
    And Registrar Tipo Tipo Cuenta "<Tipo_Cuenta>"
    And Registrar Tipo Moneda Base "<Moneda_Base>"
    And Registrar Tipo Apalancamiento "<Apalancamiento>"
    And Registrar Tipo Cantidad inversion "<Cantidad_inversion>"
    And Registrar Contrasena "<Contrasena>"
    And Registrar Confirmacion  "<Confirmacion>"
 	Then el verifica que la cuenta se halla creado 
  
 	
    Examples: 
    |Primer_Nombre|Apellido|Pais_Residencia|Ciudad|Telefono|Correo|Idioma_Preferido|Tipo_Plataforma|Tipo_Cuenta|Moneda_Base|Apalancamiento|Cantidad_inversion|Contrasena|Confirmacion|
|Pedro|Perez|Colombia|Bogota|+57 5702223|jg@gmail.com|Ruso|MT4 (forex, CFDs sobre índices bursátiles, metales, energías)|XM Ultra Low Micro (1 lote=1,000)|USD|1:400|25.000|Bogo789*|Bogo788*|

 @Reto02.4
  Scenario Outline: 4.Mediante un escenario de caso orientado al Error intentar crear una cuenta con contraseñas diferentes. 
  El valor del campo Contraseña de la cuenta y del campo 
  Confirmación de contraseña deben ser autogenerado y  se debe mostrar por la consola de ejecución. Se pueden apoyar en la función Random de java
    Given Ingresar al aplicativo Registro de Cuenta Demo
    And Registrar Nombre "<Primer_Nombre>"
    And Registrar Apellido "<Apellido>"
    And Registrar Pais Recidencia "<Pais_Residencia>"
    And Registrar Ciudad "<Ciudad>"
    And Registrar Telefono "<Telefono>"
    And Registrar Correo "<Correo>"
    And Registrar Idioma Preferido "<Idioma_Preferido>"
    And Registrar Tipo Plataforma "<Tipo_Plataforma>"
    And Registrar Tipo Tipo Cuenta "<Tipo_Cuenta>"
    And Registrar Tipo Moneda Base "<Moneda_Base>"
    And Registrar Tipo Apalancamiento "<Apalancamiento>"
    And Registrar Tipo Cantidad inversion "<Cantidad_inversion>"
    And Registrar ContrasenaRandom
    And Registrar Confirmacion Contraseña Random
 	Then el verifica que la cuenta se halla creado 

    Examples: 
    |Primer_Nombre|Apellido|Pais_Residencia|Ciudad|Telefono|Correo|Idioma_Preferido|Tipo_Plataforma|Tipo_Cuenta|Moneda_Base|Apalancamiento|Cantidad_inversion|Contrasena|Confirmacion_Contrasena|
|Pedro|Perez|Colombia|Bogota|+57 5702223|jg@gmail.com|Ruso|MT4 (forex, CFDs sobre índices bursátiles, metales, energías)|XM Ultra Low Micro (1 lote=1,000)|USD|1:400|25.000|||

