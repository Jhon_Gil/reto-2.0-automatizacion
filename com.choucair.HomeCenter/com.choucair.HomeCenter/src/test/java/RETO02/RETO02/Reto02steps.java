package RETO02.RETO02;


import net.thucydides.core.annotations.Step;

public class Reto02steps {

	Reto02pageobjet Reto02pageobjet;
	
	public String Vglobal;

	@Step
	public void ingresar_al_aplicativo() {
		String url = "https://www.xm.com/register/account/demo?lang=es";
		Reto02pageobjet.openUrl(url);
		Reto02pageobjet.coneactarse();
	}

	@Step
	public void seleccionar_nombre(String nombre) {
		// if(! Utilidades.GetValue("reti_proyecto").isEmpty())
		if (!nombre.isEmpty()) {
			Reto02pageobjet.seleccionar_Nombre(nombre);
		}
	}

	@Step
	public void registrar_apellidos(String apellido) {
		// if(! Utilidades.GetValue("reti_proyecto").isEmpty())
		if (!apellido.isEmpty()) {
			Reto02pageobjet.seleccionar_Apellido(apellido);
		}
	}

	@Step
	public void registrar_Pais(String pais) {
		if (!pais.isEmpty()) {
			Reto02pageobjet.seleccionar_Pais(pais);
		}
	}

	public void registrar_Idioma(String idioma) {
		if (!idioma.isEmpty()) {
			Reto02pageobjet.seleccionar_Idioma(idioma);
		}
	}

	@Step
	public void registrar_Ciudad(String ciudad) {
		if (!ciudad.isEmpty()) {
			Reto02pageobjet.seleccionar_Ciudad(ciudad);
		}
	}

	@Step
	public void registrar_telefono(String telefono) {
		if (!telefono.isEmpty()) {
			Reto02pageobjet.seleccionar_Telefono(telefono);
		}
	}

	@Step
	public void registrar_correo(String correo) {
		if (!correo.isEmpty()) {
			Reto02pageobjet.seleccionar_Correo(correo);
		}
	}

	@Step
	public void registrar_plataforma(String plataforma) {
		if (!plataforma.isEmpty()) {
			Reto02pageobjet.seleccionar_Plataforma(plataforma);
		}
	}

	@Step
	public void registrar_cuenta(String cuenta) {
		if (!cuenta.isEmpty()) {
			Reto02pageobjet.seleccionar_Cuenta(cuenta);
		}
	}

	@Step
	public void registrar_moneda(String moneda) {
		if (!moneda.isEmpty()) {
			Reto02pageobjet.seleccionar_Moneda(moneda);
		}
	}

	@Step
	public void registrar_apalancamiento(String apalancamiento) {
		if (!apalancamiento.isEmpty()) {
			Reto02pageobjet.seleccionar_Apalancamiento(apalancamiento);
		}
	}

	@Step
	public void registrar_inversion(String inversion) {
		if (!inversion.isEmpty()) {
			Reto02pageobjet.seleccionar_Inversion(inversion);
		}
	}

	@Step
	public void registrar_contrasena(String contrasena) {
		if (!contrasena.isEmpty()) {
			Reto02pageobjet.seleccionar_Contrasena(contrasena);
		}
	}

	@Step
	public void registrar_ccontrasena(String ccontrasena) {
		if (!ccontrasena.isEmpty()) {
			Reto02pageobjet.seleccionar_CContrasena(ccontrasena);

		}
	}

	@Step
	public void registrar_ccontrasena2(String cccontrasena) {
		if (!cccontrasena.isEmpty()) {
			Reto02pageobjet.seleccionar_CContrasena2(cccontrasena);

		}
	}

	@Step
	public void contrasena_random() {
		Reto02pageobjet.Contrasena_Random();
	}

	@Step
	public void confirmar_contrasena_random() {
		Reto02pageobjet.Confirmar_Contrasena_Random();
	}

	@Step
	public void creacion_cuenta() {

		Reto02pageobjet.check_permisos();
		Reto02pageobjet.BotonRiesgo();
		Reto02pageobjet.GardarCita();
	}

	@Step
	public void verificar_ingreso_datos_formulario_con_errores(String nombre2) {
		Reto02pageobjet.form_con_errores(nombre2);
	}

}
