package RETO02.RETO02;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import java.util.Random;

public class Reto02definition {

	@Steps
	Reto02steps Reto02steps;
	

	@Given("^Ingresar al aplicativo Registro de Cuenta Demo$")
	public void ingresar_al_aplicativo_Registro_de_Cuenta_Demo() {
		Reto02steps.ingresar_al_aplicativo();
	}

	@Given("^Registrar Nombre \"([^\"]*)\"$")
	public void registrar_Nombre(String nombre) {
		Reto02steps.seleccionar_nombre(nombre);

	}

	@Given("^Registrar Apellido \"([^\"]*)\"$")
	public void registrar_Apellido(String apellido) {
		Reto02steps.registrar_apellidos(apellido);

	}

	@Given("^Registrar Pais Recidencia \"([^\"]*)\"$")
	public void registrar_Pais_Recidencia(String pais) {
		Reto02steps.registrar_Pais(pais);

	}

	@Given("^Registrar Idioma Preferido \"([^\"]*)\"$")
	public void registrar_Idioma_Preferido(String Idioma) {
		Reto02steps.registrar_Idioma(Idioma);
	}

	@Given("^Registrar Ciudad \"([^\"]*)\"$")
	public void registrar_Ciudad(String ciudad) {
		Reto02steps.registrar_Ciudad(ciudad);

	}

	@Given("^Registrar Telefono \"([^\"]*)\"$")
	public void registrar_Telefono(String telefono) {
		Reto02steps.registrar_telefono(telefono);

	}

	@Given("^Registrar Correo \"([^\"]*)\"$")
	public void registrar_Correo(String correo) {
		Reto02steps.registrar_correo(correo);

	}

	@Given("^Registrar Tipo Plataforma \"([^\"]*)\"$")
	public void registrar_Tipo_Plataforma(String plataforma) {
		Reto02steps.registrar_plataforma(plataforma);

	}

	@Given("^Registrar Tipo Tipo Cuenta \"([^\"]*)\"$")
	public void registrar_Tipo_Tipo_Cuenta(String cuenta) {
		Reto02steps.registrar_cuenta(cuenta);

	}

	@Given("^Registrar Tipo Moneda Base \"([^\"]*)\"$")
	public void registrar_Tipo_Moneda_Base(String moneda) {
		Reto02steps.registrar_moneda(moneda);
	}

	@Given("^Registrar Tipo Apalancamiento \"([^\"]*)\"$")
	public void registrar_Tipo_Apalancamiento(String apalancamiento) {
		Reto02steps.registrar_apalancamiento(apalancamiento);

	}

	@Given("^Registrar Tipo Cantidad inversion \"([^\"]*)\"$")
	public void registrar_Tipo_Cantidad_inversion(String inversion) {
		Reto02steps.registrar_inversion(inversion);

	}

	@Given("^Registrar Contrasena \"([^\"]*)\"$")
	public void registrar_Contrasena(String contrasena) {
		Reto02steps.registrar_contrasena(contrasena);
	}

	@Given("^Registrar Confirmacion Contraseña \"([^\"]*)\"$")
	public void registrar_Confirmacion_Contraseña(String ccontrasena) {
		Reto02steps.registrar_ccontrasena(ccontrasena);

	}

	@Then("^el verifica que la cuenta se halla creado$")
	public void el_verifica_que_la_cuenta_se_halla_creado() {
		Reto02steps.creacion_cuenta();

	}

	@Given("^Error Campo Nombre \"([^\"]*)\"$")
	public void error_Campo_Nombre(String nombre2) {
		Reto02steps.verificar_ingreso_datos_formulario_con_errores(nombre2);

	}

	@Given("^Registrar Confirmacion  \"([^\"]*)\"$")
	public void registrar_Confirmacion(String cccontrasena) {
		Reto02steps.registrar_ccontrasena2(cccontrasena);

	}

	@Given("^Registrar ContrasenaRandom$")
	public void registrar_ContrasenaRandom() {
		Reto02steps.contrasena_random();

	}

	@Given("^Registrar Confirmacion Contraseña Random$")
	public void registrar_Confirmacion_Contraseña_Random() {
		Reto02steps.confirmar_contrasena_random();

	}

}
