package RETO02.RETO02;

import static org.junit.Assert.fail;
import java.security.SecureRandom;
import java.util.Random;
import java.math.BigInteger;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;


import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementState;

public class Reto02pageobjet extends PageObject {

	public String btnconectarse = "//*[@id=\'cookieModal\']/div/div/div[1]/div[2]/div[2]/div/button";
	public String lblnombre = "//*[@id=\'first_name\']";
	public String lblapellido = "//*[@id=\'last_name\']";
	public String lblpais = "//*[@id=\'country\']";
	public String lblciudad = "//*[@id='city']";
	public String lbltelefono = "//*[@id=\'phone_number\']";
	public String lblcorreo = "//*[@id=\'email\']";
	public String lblidioma = "//*[@id=\'preferred_language\']";
	public String lblplataforma = "//*[@id=\'trading_platform_type\']";
	public String lblcuenta = "//*[@id=\'account_type\']";
	public String lblmoneda = "//*[@id=\'account_currency\']";
	public String lblapalancamiento = "//*[@id=\'account_leverage\']";
	public String lblinversion = "//*[@id=\'investment_amount\']";
	public String lblcontrasena = "//*[@id=\'account_password\']";
	public String lblccontrasena = "//*[@id=\'account_password_confirmation\']";
	public String checkboletines = "//*[@id=\'demo-form\']/div[3]/div[14]";
	public String btnguardarregistro = "//*[@id=\'submit-btn\']";
	public String ErrorNombre = "//div[@class='form-group has-feedback has-error']//label[@for='first_name'][ @class='error']";
	public String ErrorCont1 =  "//div[@class='form-group has-feedback has-error']//label[@for='account_password_confirmation'][ @class='error']"; 
	public String ErrorCont2 =  "//div[@class='form-group has-feedback has-error']//label[@for='account_password_confirmation'][ @class='error']";
	public String botonRiesgo = "//*[@id=\"js-riskCloseButton\"]";


	public void coneactarse() {
		try {
			Serenity.takeScreenshot();
			find(By.xpath(btnconectarse)).click();
		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo coneactarse: " + e);
		}
	}

	public void seleccionar_Nombre(String nombre) {
		try {
			if (!nombre.isEmpty()) {

				posicionarElementoString(lblnombre);
			   find(By.xpath(lblnombre)).isVisible();
				//find(By.xpath(lblnombre)).isCurrentlyVisible();
				find(By.xpath(lblnombre)).clear();
				find(By.xpath(lblnombre)).sendKeys(nombre);
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Nombre: " + e);
		}

	}

	public void seleccionar_Apellido(String Apellido) {
		try {
			if (!Apellido.isEmpty()) {

				posicionarElementoString(lblapellido);
				//waitFor(2).second();
				 find(By.xpath(lblapellido)).isVisible();
				find(By.xpath(lblapellido)).clear();
				find(By.xpath(lblapellido)).sendKeys(Apellido);
				find(By.xpath(lblapellido)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Apellido: " + e);
		}

	}

	public void seleccionar_Pais(String pais) {
		try {
			if (!pais.isEmpty()) {
				posicionarElementoString(lblpais);
				find(By.xpath(lblpais)).isVisible();
				find(By.xpath(lblpais)).click();
				find(By.xpath(lblpais)).sendKeys(pais);
				find(By.xpath(lblpais)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Pais: " + e);
		}

	}

	public void seleccionar_Idioma(String idioma) {
		try {
			if (!idioma.isEmpty()) {

				posicionarElementoString(lblidioma);
				 find(By.xpath(lblidioma)).isVisible();
				waitFor(getDriver().findElements(By.xpath(lblidioma)).get(0)).click();
				find(By.xpath(lblidioma)).sendKeys(idioma);
				find(By.xpath(lblidioma)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Idioma: " + e);
		}

	}

	public void seleccionar_Ciudad(String ciudad) {
		try {
			if (!ciudad.isEmpty()) {
				posicionarElementoString(lblciudad);
				 find(By.xpath(lblciudad)).isVisible();
				posicionarElementoString(lblciudad);
				waitFor(getDriver().findElements(By.xpath(lblciudad)).get(0)).click();
				find(By.xpath(lblciudad)).sendKeys(ciudad);
				find(By.xpath(lblciudad)).click();

			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Ciudad: " + e);
		}

	}

	public void seleccionar_Telefono(String telefono) {
		try {
			if (!telefono.isEmpty()) {
				posicionarElementoString(lbltelefono);
				 find(By.xpath(lbltelefono)).isVisible();
				posicionarElementoString(lbltelefono);
				find(By.xpath(lbltelefono)).clear();
				find(By.xpath(lbltelefono)).sendKeys(telefono);
				find(By.xpath(lbltelefono)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Telefono: " + e);
		}

	}

	public void seleccionar_Correo(String correo) {
		try {
			if (!correo.isEmpty()) {
				posicionarElementoString(lblcorreo);
				 find(By.xpath(lblcorreo)).isVisible();
				posicionarElementoString(lblcorreo);
				waitFor(getDriver().findElements(By.xpath(lblcorreo)).get(0)).click();
				find(By.xpath(lblcorreo)).clear();
				find(By.xpath(lblcorreo)).sendKeys(correo);
				find(By.xpath(lblcorreo)).click();

			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Correo: " + e);

		}

	}

	public void seleccionar_Plataforma(String plataforma) {
		try {
			if (!plataforma.isEmpty()) {
				posicionarElementoString(lblplataforma);
				 find(By.xpath(lblcorreo)).isVisible();
				 find(By.xpath(lblcorreo)).isVisible();
				posicionarElementoString(lblplataforma);
				find(By.xpath(lblplataforma)).click();
				find(By.xpath(lblplataforma)).sendKeys(plataforma);
				find(By.xpath(lblplataforma)).click();

			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Plataforma: " + e);

		}

	}

	public void seleccionar_Cuenta(String cuenta) {
		try {
			if (!cuenta.isEmpty()) {
				posicionarElementoString(lblcuenta);
				 find(By.xpath(lblcuenta)).isVisible();
				posicionarElementoString(lblcuenta);
				waitFor(getDriver().findElements(By.xpath(lblcuenta)).get(0)).click();
				find(By.xpath(lblcuenta)).sendKeys(cuenta);
				find(By.xpath(lblcuenta)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Cuenta: " + e);

		}

	}

	public void seleccionar_Moneda(String moneda) {
		try {
			if (!moneda.isEmpty()) {
				posicionarElementoString(lblmoneda);
				find(By.xpath(lblmoneda)).isVisible();
				posicionarElementoString(lblmoneda);
				find(By.xpath(lblmoneda)).sendKeys(moneda);
				find(By.xpath(lblmoneda)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Moneda: " + e);

		}

	}

	public void seleccionar_Apalancamiento(String apalancamiento) {
		try {
			if (!apalancamiento.isEmpty()) {
				posicionarElementoString(lblapalancamiento);
				find(By.xpath(lblapalancamiento)).isVisible();
				posicionarElementoString(lblapalancamiento);
				find(By.xpath(lblapalancamiento)).sendKeys(apalancamiento);
				find(By.xpath(lblapalancamiento)).click();

			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Apalancamiento: " + e);

		}

	}

	public void seleccionar_Inversion(String inversion) {
		try {
			if (!inversion.isEmpty()) {
				posicionarElementoString(lblinversion);
				find(By.xpath(lblinversion)).isVisible();
				posicionarElementoString(lblinversion);
				find(By.xpath(lblinversion)).sendKeys(inversion);
				find(By.xpath(lblinversion)).click();

			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Inversion: " + e);

		}

	}

	public void seleccionar_Contrasena(String contrasena) {
		try {
			if (!contrasena.isEmpty()) {
				posicionarElementoString(lblcontrasena);
				find(By.xpath(lblcontrasena)).isVisible();
				posicionarElementoString(lblcontrasena);
				find(By.xpath(lblcontrasena)).sendKeys(contrasena);
				find(By.xpath(lblcontrasena)).click();
			}

		} catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_Contrasena " + e);

		}

	}

	public void seleccionar_CContrasena(String ccontrasena) {
		try {
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).isVisible();
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).sendKeys(ccontrasena);
			waitFor(2).second();
		}

		catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo seleccionar_CContrasena: " + e);

		}
	}

	public void seleccionar_CContrasena2(String cccontrasena) {
		try {
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).isVisible();
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).typeAndTab(cccontrasena);
			waitFor(2).second();

			if (find(By.xpath(ErrorCont1)).getTextValue().equals("Las contraseñas deben coincidir")) {

				find(By.xpath(lblccontrasena)).clear();
				waitFor(1).second();

				if (find(By.xpath(ErrorCont2)).getTextValue()
						.equals("El campo Confirmación de contraseña es requerido")) {

					find(By.xpath(lblccontrasena)).typeAndTab("");
					waitFor(1).second();
				}
			}
		}

		catch (Exception e) {
			fail("error en la clase Reto02pageobjet , en el metodo seleccionar_CContrasena2: " + e);

		}
	}

	public static String generarcontrasenaaleatoria() {
		String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#[]()@$&*!?|%,.^/\\+_-";
		String pw = RandomStringUtils.random(15, caracteres);
		return pw;
	}

	public void Contrasena_Random() {

		Reto02pageobjet pro = new Reto02pageobjet();
		try {
			posicionarElementoString(lblcontrasena);
			find(By.xpath(lblcontrasena)).isVisible();
			posicionarElementoString(lblcontrasena);
			find(By.xpath(lblcontrasena)).sendKeys(pro.generarcontrasenaaleatoria());
			System.out.println("Contraseña Auto Generada: " + pro.generarcontrasenaaleatoria());
			waitFor(2).second();

		}

		catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo Contrasena_Random: " + e);

		}
	}

	public void Confirmar_Contrasena_Random() {

		Reto02pageobjet pro = new Reto02pageobjet();
		try {
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).isVisible();
			posicionarElementoString(lblccontrasena);
			find(By.xpath(lblccontrasena)).sendKeys(pro.generarcontrasenaaleatoria());
			System.out.println("Comfirmación Contraseña Auto Generada: " + pro.generarcontrasenaaleatoria());
			waitFor(2).second();
		}

		catch (Exception e) {
			fail("error en la clase Reto02pageobjet, en el metodo Confirmar_Contrasena_Random: " + e);

		}
	}

	public void check_permisos() {

		find(By.xpath(checkboletines)).click();
	}
	public void BotonRiesgo() {
		find(By.xpath(botonRiesgo)).click();

	}

	public void GardarCita() {
		find(By.xpath(btnguardarregistro)).click();

	}

	public void form_con_errores(String nombre2) {

		try {
			if (find(By.xpath(ErrorNombre)).getTextValue().equals("Complete el campo Primer Nombre usando solo caracteres del alfabeto ingles")) {
				seleccionar_Nombre(nombre2);
			}

		} catch (Exception e) {
			System.out.println("error en la clase Reto02pageobjet, en el metodo form_con_errores:" + e);
		}

	}

	public void posicionarElementoString(String Element) {
		((JavascriptExecutor) getDriver()).executeScript(
				"arguments[0].scrollIntoView(true); arguments[0].style.border='1px dashed red';",
				getDriver().findElements(By.xpath(Element)).get(0));
	}

}
